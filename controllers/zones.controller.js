const Router = require('express').Router();
const mongoose = require('mongoose');
const Zone = mongoose.model('Zone');
const getErrorMessage = require('../controllers/error-message.controller');

// Haversine fomula for getting a distance between two coordinates
function getDistanceInMeters(lat1, lng1, lat2, lng2) {
	const R = 6371000;
	let dLat = deg2rad(lat2 - lat1);
	let dLng = deg2rad(lng2 - lng1);
	let a = Math.sin(dLat/2) * Math.sin(dLat/2) +
			Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
			Math.sin(dLng/2) * Math.sin(dLng/2);
	let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
	
	return R * c;
}

function deg2rad(deg) {
	return deg * (Math.PI / 180);
}

Router.get('/', (req, res) => {
	if (req.query) {
		let lat = req.query['lat'];
		let lng = req.query['lng'];
		let positionCoordinates = [Number(lng), Number(lat)];
		
		Zone.where('center.coordinates')
			.near({ center: positionCoordinates, maxDistance: 1 })
			.exec((err, zones) => {
				if (err) {
					return res.status(400).send({ message: getErrorMessage(err) });
				} else {
					let myZone;
					let len = zones.length;
					
					if (len > 1) {
						let distances = [];
						
						for (let i = 0; i < len; i++) {
							let zoneLat = zones[i].center.coordinates[1];
							let zoneLng = zones[i].center.coordinates[0];
							let rad = zones[i].radius;
							let dist = getDistanceInMeters(lat, lng, zoneLat, zoneLng);
							let nearBy = Math.floor(rad - dist);
							
							distances.push(nearBy);
						}

						// Getting the index of a greatest number
						let index = distances.reduce((prev, cur, i, arr) => {
							return cur > arr[prev] ? i : prev;
						}, 0);
						
						myZone = zones[index];
					} else {
						myZone = zones;
					}
					
					res.status(200).json(myZone);
				}
			});
	} else {
		let message = 'No query';
		
		return res.status(400).send({ message: message });
	}
});

Router.post('/', (req, res) => {
	let zone = new Zone(req.body);
	
	zone.save((err) => {
		if (err) {
			return res.status(400).send({ message : getErrorMessage(err) });
		} else {
			res.status(200).json(zone);
		}
	});
});

Router.param('zoneId', (req, res, next, id) => {
	Zone.findById(id)
		.exec((err, zone) => {
			if (err) return next(err);
			if (!zone) return next(new Error(`Failed to load zone ${ id }`));
			
			req.zone = zone;
			next();
		});
});

Router.route('/:zoneId')
	.get((req, res) => res.status(200).json(req.zone))
	.put((req, res) => {
		let zone = req.zone;
		
		zone.name = req.body.name;
		zone.center = req.body.center;
		zone.radius = req.body.radius;
		zone.address = req.body.address;
		
		zone.save((err) => {
			if (err) {
				return res.status(400).send({ message : getErrorMessage(err) });
			} else {
				res.status(200).json(zone);
			}
		});
	})
	.delete((req, res) => {
		let zone = req.zone;
		
		zone.remove((err) => {
			if (err) {
				return res.status(400).send({ message: getErrorMessage(err) });
			} else {
				res.status(200).json(zone);
			}
		});
	});

module.exports = Router;
