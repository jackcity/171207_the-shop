const Router = require('express').Router();
const mongoose = require('mongoose');
const Shop = mongoose.model('Shop');
const Keyword = mongoose.model('Keyword');
const Promise = require('bluebird');
const getErrorMessage = require('../controllers/error-message.controller');
const populateShops = [
	{ path: 'zone', select: 'name' },
	{ path: 'category', select: 'main' },
	{ path: 'products.bargain', select: ['isBargaining', 'title', 'description', 'time'] }
];

Router.post('/search', (req, res) => {
	if (req.body) {
		console.log('req.body in shopsCtrl : ', req.body);
		
		let zoneId = req.body.zoneId ? req.body.zoneId : '5996b97df9f6d7dde5a3e7ab';
		let shopIds = req.body.shopIds ? req.body.shopIds : '';
		let swLat = req.body.swLat ? req.body.swLat : '37.271725';
		let swLng = req.body.swLng ? req.body.swLng : '127.013767';
		let neLat = req.body.neLat ? req.body.neLat : '37.276344';
		let neLng = req.body.neLng ? req.body.neLng : '127.017415';
		let lowerLeft = [swLng, swLat];
		let upperRight = [neLng, neLat];
		let shopsQuery;
		
		if (shopIds.length) {
			shopsQuery = Shop.find({ _id: { $in: shopIds } });
		} else {
			shopsQuery = Shop.find({ zone: zoneId });
		}
		
		shopsQuery
			.where('location').within().box(lowerLeft, upperRight)
			.populate(populateShops)
			.exec((err, shops) => {
				if (err) {
					return res.status(400).send({ message: getErrorMessage(err) });
				} else {
					Promise.map(shops, (shop) => {
						let keywordsQuery = Keyword
							.find({ shop: shop._id })
							.sort({ count: -1 })
							.limit(10)
							.exec();
						
						return keywordsQuery.then((keywords) => {
							let len = keywords.length;
							
							for (let i = 0; i < len; i++) {
								if (keywords[i].hashKeyword === true) {
									shop.keywords.hashKeywords.push(keywords[i].keyword);
								} else {
									shop.keywords.shopKeywords.push(keywords[i].keyword);
								}
							}
							
							return shop;
						}).catch(err => err);
					})
					.then((results) => res.status(200).json(results))
					.catch((err) => console.log(`error : ${ err }`));
				}
			});
	} else {
		let message = 'No body';
		
		return res.status(400).send({ message: message });
	}
});

Router.post('/', (req, res) => {
	let shop = new Shop(req.body);
	
	shop.save((err) => {
		if (err) {
			return res.status(400).send({ message : getErrorMessage(err) });
		} else {
			res.status(200).json(shop);
		}
	});
});

Router.param('shopId', (req, res, next, id) => {
	Shop.findById(id)
		.exec((err, shop) => {
			if (err) return next(err);
			if (!shop) return next(new Error(`Failed to load shop ${ id }`));
			
			req.shop = shop;
			next();
		});
});

Router.route('/:shopId')
	.get((req, res) => res.status(200).json(req.shop))
	.put((req, res) => {
		let shop = req.shop;
		
		shop.zone = req.body.zone ? req.body.zone : shop.zone;
		shop.owner = req.body.owner ? req.body.owner : shop.owner;
		shop.location = req.body.location ? req.body.location : shop.location;
		shop.image = req.body.image ? req.body.image : shop.image;
		shop.name = req.body.name ? req.body.name : shop.name;
		shop.category = req.body.category ? req.body.category : shop.category;
		shop.keywords = req.body.keywords ? req.body.keywords : shop.keywords;
		shop.address = req.body.address ? req.body.address : shop.address;
		shop.phone = req.body.phone ? req.body.phone : shop.phone;
		shop.products = req.body.products ? req.body.products : shop.products;

		shop.save((err) => {
			if (err) {
				return res.status(400).send({ message: getErrorMessage(err) });
			} else {
				res.status(200).json(shop);
			}
		});
	})
	.delete((req, res) => {
		let shop = req.shop;
		
		shop.remove((err, shop) => {
			if (err) {
				return res.status(400).send({ message: getErrorMessage(err) });
			} else {
				res.status(200).json(shop);
			}
		});
	});

module.exports = Router;