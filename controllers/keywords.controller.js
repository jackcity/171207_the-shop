const Router = require('express').Router();
const mongoose = require('mongoose');
const Keyword = mongoose.model('Keyword');
const Shop = mongoose.model('Shop');
const Promise = require('bluebird');
const getErrorMessage = require('../controllers/error-message.controller');

Router.get('/', (req, res) => {
	if (req.query) {
		console.log('req.query in hashesCtrl : ', req.query);
		
		let zoneId = req.query['zone-id'] ? req.query['zone-id'] : '5996b97df9f6d7dde5a3e7ab';
		let aggregationQuery;
		
		if (req.query['search']) {
			let keyword = req.query['search'];
			let regexKeyword = new RegExp(`^${ keyword }`);
			
			aggregationQuery = Keyword.aggregate([
				{ $match: {
					$and: [
						{ zone: mongoose.Types.ObjectId(zoneId) },
						{ keyword: { $regex: regexKeyword } }
					]
				}},
				{ $group: {
					_id: '$keyword',
					shops: { $addToSet: '$shop' },
					hashKeyword: { $push: '$hashKeyword' }
				}}
			]);
		} else if (req.query['latest']) {
			let keyword = req.query['latest'];
			
			aggregationQuery = Keyword.aggregate([
				{ $match: {
					$and: [
						{ zone: mongoose.Types.ObjectId(zoneId) },
						{ keyword: keyword }
					]
				}},
				{ $group: {
					_id: '$keyword',
					shops: { $addToSet: '$shop' },
					hashKeyword: { $push: '$hashKeyword' }
				}}
			]);
		} else {
			let message = 'No keyword for search!!!';
			
			return res.status(400).send({ message: message });
		}
		
		aggregationQuery.exec((err, keywords) => {
			if (err) {
				return res.status(400).send({ message: getErrorMessage(err) });
			} else {
				console.log('result docs : ', keywords);
				
				res.status(200).json(keywords);
			}
		});
	} else {
		let message = 'No query';
		
		return res.status(400).send({ message: message });
	}
});

Router.post('/', (req, res) => {
	let keywordsObj = req.body;
	let resultsObj = {
		modifiedKeywords: [],
		upsertedKeywords: []
	};
	
	console.log('req.body : ', req.body);
	
	Promise.map(keywordsObj.keywords, (keyword) => {
		return Keyword.bulkWrite([{
			updateOne: {
				filter: {
					zone: keywordsObj.zoneId,
					shop: keywordsObj.shopId,
					keyword: { $in: keyword }
				},
				update: {
					$inc: { count: 1 },
					$set: {
						created: Date.now(),
						hashKeyword: true
					}
				},
				upsert: true
			}
		}]).then((result) => {
			if (result.nModified === 1) {
				resultsObj.modifiedKeywords.push(keyword);
			} else if (result.nUpserted === 1) {
				resultsObj.upsertedKeywords.push(keyword);
			}
		}).catch((err) => console.log(`error : ${ err }`));
	})
	.then(() => res.status(200).json(resultsObj))
	.catch((err) => console.log(`error : ${ err }`));
});

Router.param('keywordId', (req, res, next, id) => {
	Keyword.findById(id).exec((err, keyword) => {
		if (err) return next(err);
		if (!keyword) return next(new Error(`Failed to load keyword ${ id }`));
		
		req.keyword = keyword;
		next();
	});
});

Router.put('/:keywordId', (req, res) => {
	let keyword = req.keyword;
	
	keyword.hashKeyword = req.body.hashKeyword ? req.body.hashKeyword : (keyword.hashKeyword ? keyword.hashKeyword : false);
	keyword.shopKeyword = req.body.shopKeyword ? req.body.shopKeyword : (keyword.shopKeyword ? keyword.shopKeyword : false);
	
	keyword.save((err) => {
		if (err) {
			return res.status(400).send({ message: getErrorMessage(err) });
		} else {
			res.status(200).json(keyword);
		}
	});
});

module.exports = Router;
