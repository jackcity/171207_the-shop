const Router = require('express').Router();
const mongoose = require('mongoose');
const Bargain = mongoose.model('Bargain');
const getErrorMessage = require('../controllers/error-message.controller');

setInterval(() => {
	let now = new Date();
	let year = now.getFullYear();
	let month = now.getMonth();
	let date = now.getDate();
	let hour = now.getHours() + 9;
	let minute = now.getMinutes();
	let second = now.getSeconds();
	let exceededSecond = now.getSeconds() + 5;
	let currentKorDate = new Date(year, month, date, hour, minute, second);
	let exceededDate = new Date(year, month, date, hour, minute, exceededSecond);
	
	Bargain.bulkWrite([
		{
			updateMany: {
				filter: {
					isBargaining: true,
					'time.end': { $gte: currentKorDate, $lt: exceededDate }
				},
				update: {
					isBargaining: false,
					title: null,
					description: null,
					'time.start': null,
					'time.end': null
				}
			}
		},
		{
			updateMany: {
				filter: {
					isBargaining: false,
					'time.start': { $gte: currentKorDate, $lt: exceededDate }
				},
				update: {
					isBargaining: true
				}
			}
		}
	])
	.then(res => {
		console.log('res : ', res);
		console.log('currentKorDate:', currentKorDate, ',  exceededDate:', exceededDate);
	})
	.catch(e => console.log(e));
}, 1000);

Router.post('/', (req, res) => {
	let bargain = new Bargain(req.body);
	
	bargain.save((err) => {
		if (err) {
			return res.status(400).send({ message : getErrorMessage(err) });
		} else {
			res.status(200).json(bargain);
		}
	});
});

Router.param('bargainId', (req, res, next, id) => {
	Bargain.findById(id).exec((err, bargain) => {
		if (err) return next(err);
		if (!bargain) return next(new Error(`Failed to load bargain ${ id }`));
		
		req.bargain = bargain;
		next();
	});
});

Router.put('/:bargainId', (req, res) => {
	let bargain = req.bargain;
	
	bargain.zone = req.body.zone ? req.body.zone : bargain.zone;
	bargain.shop = req.body.shop ? req.body.shop : bargain.shop;
	bargain.productId = req.body.productId ? req.body.productId : bargain.productId;
	bargain.isBargaining = req.body.isBargaining ? req.body.isBargaining : bargain.isBargaining;
	bargain.title = req.body.title ? req.body.title : bargain.title;
	bargain.description = req.body.description ? req.body.description : bargain.description;
	bargain.time = req.body.time ? req.body.time : bargain.time;
	
	bargain.save((err) => {
		if (err) {
			return res.status(400).send({ message: getErrorMessage(err) });
		} else {
			res.status(200).json(bargain);
		}
	});
});

module.exports = Router;