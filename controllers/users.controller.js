const Router = require('express').Router();
const mongoose = require('mongoose');
const User = mongoose.model('User');
const getErrorMessage = require('../controllers/error-message.controller');

Router.post('/', (req, res) => {
	let user = new User(req.body);
	
	user.save((err) => {
		if (err) {
			return res.status(400).send({ message : getErrorMessage(err) });
		} else {
			res.status(200).json(user);
		}
	});
});

Router.param('userId', (req, res, next, id) => {
	User.findById(id).exec((err, user) => {
		if (err) return next(err);
		if (!user) return next(new Error(`Failed to load keyword ${ id }`));
		
		req.user = user;
		next();
	});
});

Router.put('/:userId', (req, res) => {
	let user = req.user;
	
	user.zone = req.body.zone;
	user.push = req.body.pushPermission;
	
	user.save((err) => {
		if (err) {
			return res.status(400).send({ message: getErrorMessage(err) });
		} else {
			res.status(200).json(user);
		}
	});
});

module.exports = Router;