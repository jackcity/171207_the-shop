module.exports = (err) => {
	if (err.code) {
		if (err.code === 11000) {
			return '이미 존재하는 ID입니다.';
		} else {
			console.log('db error : ', err);
			return '에러가 발생했습니다. 관리자에게 문의하세요.';
		}
	} else {
		for (let errName in err.errors) {
			if (err.errors[errName].message) return err.errors[errName].message;
		}
	}
};
