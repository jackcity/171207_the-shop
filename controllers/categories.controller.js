const Router = require('express').Router();
const mongoose = require('mongoose');
const Category = mongoose.model('Category');
const getErrorMessage = require('../controllers/error-message.controller');

Router.post('/', (req, res) => {
	let category = new Category(req.body);
	
	category.save((err) => {
		if (err) {
			return res.status(400).send({ message : getErrorMessage(err) });
		} else {
			res.status(200).json(category);
		}
	});
});

Router.param('categoryId', (req, res, next, id) => {
	Category.findById(id).exec((err, category) => {
		if (err) return next(err);
		if (!user) return next(new Error(`Failed to load keyword ${ id }`));
		
		req.category = category;
		next();
	});
});

Router.put('/:categoryId', (req, res) => {
	let category = req.category;
	
	category.main = req.body.main;
	category.subCategories = req.body.subCategories;
	
	category.save((err) => {
		if (err) {
			return res.status(400).send({ message: getErrorMessage(err) });
		} else {
			res.status(200).json(category);
		}
	});
});

module.exports = Router;