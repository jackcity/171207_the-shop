process.env.NODE_ENV = process.env.NODE_ENV || 'development';

const mongooseConfig = require('./config/mongoose');
const expressConfig = require('./config/express');

const db = mongooseConfig();
const app = expressConfig(db);
const port = process.env.PORT || 3000;

app.listen(port, () => {
	console.log(`Server running at http://localhost:${ port }/`);
});

module.exports = app;
