const config = require('./config');
const mongoose = require('mongoose');

mongoose.Promise = global.Promise;

module.exports = () => {
	// Connecting db
	const db = mongoose.connect(config.dbUri);
	
	// Connection events
	mongoose.connection.on('connected', () => {
		console.log('Mongoose default connection open to ' + config.dbUri);
	});
	mongoose.connection.on('error', console.error.bind(console, 'MongoDB connection error: '));
	mongoose.connection.on('disconnected', () => {
		console.log('Mongoose disconnected');
	});
	
	// Models
	require('../models/zone.model');
	require('../models/shop.model');
	require('../models/category.model');
	require('../models/bargain.model');
	require('../models/keyword.model');
	require('../models/user.model');
	
	return db;
};
