const config = require('./config');
const cors = require('cors');
const express = require('express');
const morgan = require('morgan');
const compress = require('compression');
const bodyParser = require('body-parser');
const methodOverride = require('method-override');
const session = require('express-session');
const MongoStore = require('connect-mongo')(session);

module.exports = function (db) {
	const app = express();
	
	if (process.env.NODE_ENV === 'development') {
		app.use(morgan('dev'));
	} else if (process.env.NODE_ENV === 'production') {
		app.use(compress());
	}
	app.use(bodyParser.urlencoded({ extended: true }));
	app.use(bodyParser.json());
	app.use(methodOverride());
	
	// Setting mongo store to save the session
	const mongoStore = new MongoStore({
		mongooseConnection: db.connection
	});
	
	app.use(session({
		store: mongoStore,
		secret: config.sessionSecret,
		resave: false,
		saveUninitialized: true
	}));
	
	// Configuring CORS with Dynamic Origin
	const whitelist = ['*', 'http://localhost:4200', 'http://localhost:8100'];
	const corsOptions = {
		origin: function(origin, callback){
			const originIsWhitelisted = whitelist.indexOf(origin) !== -1;
			callback(originIsWhitelisted ? null : 'Bad Request', originIsWhitelisted);
		},
		credentials: true
	};
	
	app.use(cors(corsOptions));
	
	// Routes and Controllers
	const zones = require('../controllers/zones.controller');
	const shops = require('../controllers/shops.controller');
	const categories = require('../controllers/categories.controller');
	const bargains = require('../controllers/bargains.controller');
	const keywords = require('../controllers/keywords.controller');
	const users = require('../controllers/users.controller');
	
	app.use('/api/zones', zones);
	app.use('/api/shops', shops);
	app.use('/api/categories', categories);
	app.use('/api/bargains', bargains);
	app.use('/api/keywords', keywords);
	app.use('/api/users', users);
	
	return app;
};