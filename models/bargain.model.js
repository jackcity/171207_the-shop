const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const BargainSchema = new Schema({
	zone: {
		type: Schema.ObjectId,
		ref: 'Zone'
	},
	shop: {
		type: Schema.ObjectId,
		ref: 'Shop'
	},
	productId: {
		type: Number,
		required: true
	},
	isBargaining: {
		type: Boolean,
		required: true
	},
	title: {
		type: String
	},
	description: {
		type: String
	},
	time: {
		start: Date,
		end: Date
	}
});

mongoose.model('Bargain', BargainSchema);
