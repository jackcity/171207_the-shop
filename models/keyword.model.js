const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const KeywordSchema = new Schema({
	zone: {
		type: Schema.ObjectId,
		ref: 'Zone'
	},
	shop: {
		type: Schema.ObjectId,
		ref: 'Shop'
	},
	keyword: {
		type: String,
		required: true,
		text: true
	},
	hashKeyword: {
		type: Boolean,
		default: false
	},
	shopKeyword: {
		type: Boolean,
		default: false
	},
	count: {
		type: Number,
		default: 1,
		index: true
	},
	created: {
		type: Date,
		default: Date.now
	}
});

mongoose.model('Keyword', KeywordSchema);