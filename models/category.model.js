const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const CategorySchema = new Schema({
	main: {
		type: String,
		required: true
	},
	subCategories: [{
		type: String
	}]
});

mongoose.model('Category', CategorySchema);
