const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const UserSchema = new Schema({
	zone: {
		type: Schema.ObjectId,
		ref: 'Zone'
	},
	uuid: {
		type: String,
		unique: true,
		required: true
	},
	model: {
		type: String,
		required: true
	},
	platform: {
		type: String,
		required: true
	},
	version: {
		type: String,
		required: true
	},
	push: {
		type: Boolean,
		required: true
	}
});

mongoose.model('User', UserSchema);
