const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ZoneSchema = new Schema({
	name: {
		type: String,
		required: true
	},
	center: {
		type: {
			type: String,
			default: 'Point',
			enum: ['Point', 'LineString', 'Polygon']
		},
		coordinates: {
			type: [Number],
			index: '2d',
			required: true
		}
	},
	radius: {
		type: Number,
		required: true
	},
	address: {
		/*
		type: Schema.ObjectId,
		ref: 'Address'
		*/
		type: String
	}
});

mongoose.model('Zone', ZoneSchema);