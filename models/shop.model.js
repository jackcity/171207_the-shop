const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ImageDetail = new Schema({
	key: {
		type: String,
		required: true
	}
}, { _id: false });
const Product = new Schema({
	productId: {
		type: Number,
		required: true
	},
	name: {
		type: String,
		required: true
	},
	price: {
		type: Number,
		required: true
	},
	bargain: {
		type: Schema.ObjectId,
		ref: 'Bargain'
	}
}, { _id: false });
const ShopSchema = new Schema({
	zone: {
		type: Schema.ObjectId,
		ref: 'Zone'
	},
	owner: {
		/*
		type: Schema.ObjectId,
		ref: 'Owner'
		*/
		username: String
	},
	location: {
		type: {
			type: String,
			default: 'Point',
			enum: ['Point', 'LineString', 'Polygon']
		},
		coordinates: {
			type: [Number],
			index: '2d',
			required: true
		}
	},
	image: {
		logo: {
			key: {
				type: String,
				required: true
			}
		},
		details: [ImageDetail]
	},
	name: {
		type: String,
		required: true,
		trim: true
	},
	category: {
		type: Schema.ObjectId,
		ref: 'Category'
	},
	keywords: {
		shopKeywords: [String],
		hashKeywords: [String]
	},
	address: {
		type: String,
		required: true
	},
	phone: {
		type: String,
		required: true
	},
	products: [Product],
	created: {
		type: Date,
		default: Date.now,
		required: true
	}
});

mongoose.model('Shop', ShopSchema);
